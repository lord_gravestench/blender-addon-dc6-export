"""
A Blender add-on which manages a camera rig and encodes renders to DC6 format.
"""

import subprocess
import bpy
from . import (properties, panel, operators)

ADDON_COMPONENTS = [properties, panel, operators]

bl_info = {
    "name": "Diablo II DC6 Export",
    "description": "Creates DC6 images for use in Diablo II",
    "author": "Gravestench",
    "version": (0, 0, 1),
    "blender": (2, 80, 0),
    "location": "3D View > Tools",
    "warning": "",
    "wiki_url": "",
    "tracker_url": "",
    "category": "Development"
}


def attempt_pillow_install():
    """Attempt to install Pillow image processing library."""
    blender_pypath = bpy.app.binary_path_python
    install_pillow = (blender_pypath, "-m", "pip", "install", "Pillow")
    subprocess.Popen(install_pillow)


def register():
    """Register this module with Blender Python API"""
    attempt_pillow_install()
    for component in ADDON_COMPONENTS:
        component.register()


def unregister():
    """Unregister this module with Blender Python API"""
    for component in ADDON_COMPONENTS:
        component.unregister()


if __name__ == '__main__':
    register()
